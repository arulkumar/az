// var vehicles = require('controllers/VehicleController');
var home = require('./controllers/HomeController')
var suggest = require('./controllers/SuggestVehicleController')

module.exports = function(app){


    //http://vlasenko.org/2011/10/12/expressconnect-static-set-last-modified-to-now-to-avoid-304-not-modified/
    // app.get('/*', function(req, res, next){
    //   res.setHeader('Last-Modified', (new Date()).toUTCString());
    //   next();
    // });

    /* Home page */
    app.get('/', home.index);
    app.get('/which-new-car-to-buy-india', suggest.index);
    app.post('/get-suggestion', suggest.create);
    app.get('/get-suggestion', suggest.show);
    // app.get('/blog', home.blog);
    // app.get('/suggest', home.suggest);

    /* New & Review */
    // app.get('/news-reviews/:slug/:id', Home.index);

    /*
     *  Blog
     */
    // app.get('/blog/create', BlogController.create);

    // app.get('/vehicle/create', VehicleController.create);

    // app.param('id', articles.load);
    // app.get('/articles', articles.index);
    // app.get('/articles/new', auth.requiresLogin, articles.new);
    // app.post('/articles', auth.requiresLogin, articles.create);
    // app.get('/articles/:id', articles.show);
    // app.get('/articles/:id/edit', articleAuth, articles.edit);
    // app.put('/articles/:id', articleAuth, articles.update);
    // app.delete('/articles/:id', articleAuth, articles.destroy);

};