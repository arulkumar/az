
var config = require('../config/Config');

exports.index = function (req, res) {
    // res.setHeader('Cache-Control', 'no-cache');
    //
    var temp = {    title: "test",
                    url: "urls" ,
                    meta_description: "Get free personalised suggestion on buying a new car in India. At AutoZoom we help you to find the right car based on your preferences.",
                    meta_keywords: "which new car to buy india, buy new car india, new car suggestion",
                    description: "desc",
                    main_image: "http://autozoom.in/images" };

    res.render('suggestion', { page : temp } );
    // res.end();
}


exports.create = function (req, res) {

    //http://stackoverflow.com/questions/7625410/render-template-to-variable-in-expressjs

    //Send Enquiry Mailer to Support team
    res.render('mailer/suggest.html', { page : req.body }, function generateSuggestionMailer(err, output) {
        if (err) throw err;
        console.log(output);
        var message = {
            "html": output,
            "subject": req.body.user.name + ", Which car to buy ?",
            "from_email": config.from_email,
            "from_name": config.from_name,
            "to": [{
                    "email": config.from_email, //req.body.user.email,
                    "name":  config.from_name, //req.body.user.name,
                    "type": "to"
                }]
        };
        sendMailer(message);
    });

    //Send Welcome email to Customer
    res.render('mailer/welcome.html', { page : req.body }, function generateWelcomeMailer(err, output) {
        if (err) throw err;
        console.log(output);
        var message = {
            "html": output,
            "subject": req.body.user.name + ", Welcome to AutoZoom",
            "from_email": "hello@autozoom.in",
            "from_name": "AutoZoom",
            "to": [{
                    "email": req.body.user.email,
                    "name": req.body.user.name,
                    "type": "to"
                }]
        };
        sendMailer(message);
    });

    //console.log(req.body.user.mobile);
    res.render('suggestion-success');
    //res.end();
}

exports.suggest = function (req, res) {
    res.render('suggestion');
    // res.end();
}

exports.show = function (req, res) {
    res.render('suggestion-success');
    // res.end();
}


/* Private functions */

function sendMailer(message) {
    var mandrill = require('mandrill-api/mandrill');
    var mandrill_client = new mandrill.Mandrill(config.mandrill_key);

    mandrill_client.messages.send({"message": message, "async": false, "ip_pool": null, "send_at": null}, function(result) {
        console.log(result);
    }, function(e) {
        // Mandrill returns the error as an object with name and message keys
        console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
        // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    });
}