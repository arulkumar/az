
exports.index = function (req, res) {
    // res.setHeader('Cache-Control', 'no-cache');
    //
    var temp = {    title: "test",
                    url: "urls" ,
                    meta_description: "Get free personalised suggestion on buying a new car in India. At AutoZoom we help you to find the right car based on your preferences.",
                    meta_keywords: "which new car to buy india, buy new car india, new car suggestion",
                    description: "desc",
                    main_image: "http://autozoom.in/images" };

    res.render('home', { page : temp } );
    // res.end();
}


exports.blog = function (req, res) {
    res.render('blog');
    // res.end();
}

exports.suggest = function (req, res) {
    res.render('suggestion');
    // res.end();
}