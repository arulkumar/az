var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var blogSchema = new Schema ({
    id : Number,
    user_id : Number,
    title : String,
    slug : String,
    meta_description : String,
    meta_keywords : String,
    content : String,
    main_image : String,
    summary : String,
    extract : String,
    status : Number, // 0 - Draft, 1 - Published
    created_at : Date,
    updated_at : Date
});

module.exports = mongoose.model ('Blog', blogSchema);