// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');

var Xray = require('x-ray');
var x = Xray();

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });
});

router.get('/d', function(req,res) {
    //x('http://indianautosblog.com/2015/08/hyundai-creta-review-185535', 'title').write('results.json');
    //x('http://www.carwale.com/hyundai-cars/creta/16sxo-4132/', '#tbOverview>tr', [{ val : 'td', val : '>td' }]).write('results.json');
    //x('http://www.carwale.com/hyundai-cars/creta/16sxo-4132/', '#tbOverview>tr', [{  val : 'td' }]).write('results.json');
    //Perfect
    //x('http://www.carwale.com/hyundai-cars/creta/16sxo-4132/', '#tbOverview', x('tr',['td'])).write('results.json');
    //x('http://www.carwale.com/hyundai-cars/creta/16sxo-4132/', 'table#tbOverview', x('tr',[['td']])).write('results.json');
    var readStream = x('http://www.carwale.com/hyundai-cars/creta/16sxo-4132/', 'table#tbOverview', x('tr',[['td']])).write();
    var temp = '';
    readStream.on('data', function (chunk) {
        temp = temp +  chunk;
    }) .on('end', function () {
        //res.json(JSON.parse(temp));
        var json = JSON.parse(temp);
        for (var i = 0; i < json.length; i++) {
            console.log(json[i]);
        };
        res.end();
    });

});

// x('https://dribbble.com', 'li.group', [{
//   title: '.dribbble-img strong',
//   image: '.dribbble-img [data-src]@data-src',
// }])
//   .paginate('.next_page@href')
//   .limit(3)
//   .write('results.json');



// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);