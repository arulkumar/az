
/*  ============
    = Setup ====
    ============ */
var fs = require('fs');
var path = require('path');

var express = require('express');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var db = require('./app/config/database');
var swig = require('swig');
var config = require('./app/config/config');

/* Swig templating engine */
app.engine('html', swig.renderFile);
app.set('view engine','html');
app.set('views',__dirname + '/app/views');
// Swig will cache templates for you, but you can disable
// that and use Express's caching instead, if you like:
 app.set('view cache', false);
//app.disable('view cache');

// To disable Swig's cache, do the following:
swig.setDefaults({ cache: false });
// NOTE: You should always cache templates in a production environment.
// Don't leave both of these to `false` in production!

// app.set('etag',false);

// Static files middleware
app.use(express.static(path.join(__dirname, 'public')));

// Bootstrap models & Controller
fs.readdirSync(path.join(__dirname, 'app/models')).forEach(function (file) {
  if (~file.indexOf('.js')) require(path.join(__dirname, 'app/models', file));
});

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

// Bootstrap routes
require('./app/routes')(app);

/*  ====================
    = Configuration ====
    ==================== */
mongoose.connect(db.url)

// Listen to port 8080
app.listen(8080)
